package Steps;

import Pages.Applicatie;
import Pages.NetworkWebshopsPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ManageNetworkSteps extends DriverFactory {

    // show deactivate button
    @Given("^That the user is in the Webshops screen$")
    public void thatTheUserIsInTheWebshopsScreen() throws Throwable {
        new Applicatie(driver).openNetwerkSchermEnMaximaliseer();
    }

    @And("^The webshop has an active connection$")
    public void theWebshopHasAnActiveConnection() throws Throwable {
        new NetworkWebshopsPage(driver).assertFieldWebshopLuisterrijk();
        new NetworkWebshopsPage(driver).assertFieldStartContractLuisterrijk();
    }

    @Then("^Deactivation button is visible$")
    public void deactivationButtonIsVisible() throws Throwable {
        new NetworkWebshopsPage(driver).assertButtonEndContract();
    }


    // (MARLEEN) deactivate network

    @When("^The user ends the connection$")
    public void theUserEndsTheConnection()throws Throwable {
        new NetworkWebshopsPage(driver).clickButtonEndContract();
        new NetworkWebshopsPage(driver).clickPopupEndDateField();
        Thread.sleep(1000,100);
        new NetworkWebshopsPage(driver).fillTomorrowsDate();
        Thread.sleep(1000,100);
        new NetworkWebshopsPage(driver).fillReasonEndContract();
        Thread.sleep(1000,100);
        new NetworkWebshopsPage(driver).clickPopupSaveEndContract();
        Thread.sleep(5000,100);
    }


    @Then("^The contract ends on the end date$")
    public void theContractEndsOnTheEndDate()throws Throwable{
        new NetworkWebshopsPage(driver).assertEndDateFilledLuisterrijk();
    }


    // Thread.sleep(1000);

    // (KEERAN)

}
