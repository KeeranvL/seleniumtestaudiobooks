package Steps;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class DriverFactory {
    protected static WebDriver driver;
    String nodeUrl;

    public DriverFactory() {
        this.initialize();
    }

    public void initialize() {
        if (driver == null) {
            this.createNewDriverInstance();
        }

    }

    private void createNewDriverInstance() {

        String path = System.getProperty("user.dir");
        String headless = "0";
        System.setProperty("webdriver.chrome.driver", path+"\\resources\\chromedriver.exe");
        if (headless.equalsIgnoreCase("1")) {
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("--no-sandbox");
            chromeOptions.addArguments("--headless");
            chromeOptions.addArguments("--disable-gpu");
            chromeOptions.addArguments("--start-maximized");
            chromeOptions.addArguments("--window-size=1080,1920");
            chromeOptions.addArguments("--lang=nl");
            driver = new ChromeDriver(chromeOptions);
        } else {
            driver = new ChromeDriver();
        }

    }

    public WebDriver getDriver() {
        return driver;
    }

    public void destroyDriver() {
        driver.quit();
        driver = null;
    }
}
