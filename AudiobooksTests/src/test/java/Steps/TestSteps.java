package Steps;

import Pages.Applicatie;
import Pages.LoginPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TestSteps extends DriverFactory{

    @Given("^That the user is logged in as De bezige Bij.$")
    public void ThatTheUserIsLoggedInAsDeBezigeBij() throws Throwable {
        new Applicatie(driver).openUitgeverijEnMaximaliseer();
        new LoginPage(driver).directInloggenAlsBezigeBij();
    }

    @When("^hier komt nog een when$")
    public void HierKomtNogEenWhen() throws Throwable {
    }

    @Then("^hier komt nog een then$")
    public void HierKomtNogEenThen() throws Throwable {
    }

}