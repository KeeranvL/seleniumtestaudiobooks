package Steps;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;

public class StartingSteps {
    private WebDriver driver;

    public StartingSteps() {
    }

    @Before
    public void beforeScenario() {
        this.driver = (new DriverFactory()).getDriver();
    }

    @After
    public void afterScenario() {
        (new DriverFactory()).destroyDriver();
    }
}
