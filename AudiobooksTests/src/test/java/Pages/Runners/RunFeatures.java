package Pages.Runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/resources/"},
        glue = {"Steps"},
        tags = {"@Regressie"},
        plugin = {"pretty", "html:c:/cucumber-reports", "json:target/cucumber.json"},
        monochrome = true,
        dryRun = false
)
public class RunFeatures {
    public RunFeatures() {
    }
}
