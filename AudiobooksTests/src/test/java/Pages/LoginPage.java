package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    WebDriver driver;
    @FindBy(
            xpath = "//input[@id='P101_USERNAME']"
    )
    private WebElement veldGebruikersnaam;
    @FindBy(
            xpath = "//input[@id='P101_PASSWORD']"
    )
    private WebElement veldWachtwoord;
    @FindBy(
            xpath = "//a[@class='InloggenText']"
    )
    private WebElement knopInloggen;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void vulVeldGebruikersnaam(String gebruikersnaam) throws Throwable {
        this.veldGebruikersnaam.sendKeys(gebruikersnaam);
    }

    public void vulVeldWachtwoord(String wachtwoord) throws Throwable {
        this.veldWachtwoord.sendKeys(wachtwoord);
    }

    public void klikInloggenKnop() throws Throwable {
        this.knopInloggen.click();
    }

    public void directInloggenAlsBezigeBij() throws Throwable {
        this.veldGebruikersnaam.sendKeys("CBU-1045");
        this.veldWachtwoord.sendKeys("testteam");
        this.knopInloggen.click();
    }

}
