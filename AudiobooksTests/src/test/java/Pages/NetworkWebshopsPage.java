package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class NetworkWebshopsPage {

    WebDriver driver;

// Elements of the page

    // Table Field Webshop Luisterrijk (inside table)
    @FindBy(xpath = "//tr[5]//td[1]//div//div//span")
    private WebElement fieldWebshopLuisterrijk;

    // Table Field startdate contract (inside table)
    @FindBy(xpath = "//tr[5]//td[2]//div//div//span")
    private WebElement fieldStartContractLuisterrijk;

    // Table Field end date contract (inside table)
    @FindBy(xpath = "//tr[5]//td[3]//div//div//span")
    private WebElement fieldEndContractLuisterrijk;

    // Table Button to activate contract with 123Luisterboek.nl
    @FindBy(xpath = "//tr[1]//td[2]//button[@class='btn btn-InTable']")
    private WebElement buttonActivateContract123Luisterboek;

    // Table Button to end contract with Luisterrijk
    @FindBy(xpath = "//tr[5]//td[3]//button[@class='btn btn-InTable']")
    private WebElement buttonEndContractLuisterrijk;

    // Popup Date datepicker field to activate/deactivate connection
    @FindBy(xpath = "//input[@id='b3-InputEndDate']")
    private WebElement inputDatePickerPopup;

    // Popup Reason field in popup to deactivate connection
    @FindBy(xpath = "//textarea[@id='b3-TextArea_Reason']")
    private WebElement inputReasonEndContract;

    // Popup Field to click outside of datepicker (to close datepicker)
    @FindBy(xpath = "//form[@id='b4-UpdateAgreementForm']']")
    private WebElement popupOutsideDatePicker;

    // Popup Save button to activate/deactivate connection
    @FindBy(xpath = "//button[@class='btn btn-primary ThemeGrid_MarginGutter']")
    private WebElement buttonPopupSave;

    // Popup Cancel button to activate/deactivate connection
    @FindBy(xpath = "//button[@class='btn']")
    private WebElement buttonPopupCancel;


    // Actions
    // import driver
    public NetworkWebshopsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    // Asserts
    //checks if field 'Luisterrijk' in table is visible
    public void assertFieldWebshopLuisterrijk() throws Throwable {
        Assert.assertEquals(fieldWebshopLuisterrijk.getText(),"Luisterrijk");
    }

    // Checks if there is text in field 'Start Contract'
    public void assertFieldStartContractLuisterrijk() throws Throwable {
        Assert.assertNotNull(fieldStartContractLuisterrijk.getText());
    }

    // Checks if End Contract button is clickable (and thus also visible)
    public void assertButtonEndContract() throws Throwable {
        new WebDriverWait(driver, 60).until(ExpectedConditions.elementToBeClickable(buttonEndContractLuisterrijk));
        Assert.assertTrue(buttonEndContractLuisterrijk.isEnabled());
    }

    // Actions
    // Clicks button to open end contract popup
    public void clickButtonEndContract() {
        new WebDriverWait(driver, 60).until(ExpectedConditions.elementToBeClickable(buttonEndContractLuisterrijk));
        buttonEndContractLuisterrijk.click();
    }

    // Clicks input end date field in popup >> probably not necessary to use
    public void clickPopupEndDateField() {
        new WebDriverWait(driver, 60).until(ExpectedConditions.elementToBeClickable(inputDatePickerPopup));
        inputDatePickerPopup.click();
    }

    // Gets current date and reformats + fills it
    public void fillTomorrowsDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy"); // formatter for date
        LocalDate localDateTomorrow = LocalDate.now().plusDays(1); // gets tomorrow's date from
        String tomorrowUnformatted = dtf.format(localDateTomorrow); // places LocalDate in correct format in string tomorrow
        String tomorrow = tomorrowUnformatted.substring(0,7);
        inputDatePickerPopup.sendKeys(tomorrow);
    }

    public void fillReasonEndContract() {
        inputReasonEndContract.click();
        inputReasonEndContract.sendKeys("Testreden");
    }

    public void clickPopupSaveEndContract() {
        buttonPopupSave.click();
    }

    public void assertEndDateFilledLuisterrijk() throws Throwable {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy"); // formatter for date
        LocalDate localDateTomorrow = LocalDate.now().plusDays(1); // gets tomorrow's date from
        String tomorrow = dtf.format(localDateTomorrow); // places LocalDate in correct format in string tomorrow

        new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOf(fieldEndContractLuisterrijk));
        Assert.assertEquals(fieldEndContractLuisterrijk.getText(),tomorrow);
    }


}
