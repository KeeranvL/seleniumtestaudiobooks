package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class Applicatie {
    WebDriver driver;

    public Applicatie(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void openUitgeverijEnMaximaliseer() throws Throwable {
        String omgeving = "tst";
        this.driver.get("https://cbonline" + omgeving + ".boekhuis.nl/pls/start/f?p=400");
        this.driver.manage().window().maximize();
        WebElement html = this.driver.findElement(By.tagName("html"));
        html.sendKeys(Keys.CONTROL, "0");
    }

    public void openNetwerkSchermEnMaximaliseer() throws Throwable {
        String omgeving = "tst2";
        this.driver.get("https://cb-" + omgeving + ".outsystemsenterprise.com/Audiobooks/Webshops?PublisherId=2&Token=wjNVUS8477U64uOh");
        this.driver.manage().window().maximize();
        WebElement html = this.driver.findElement(By.tagName("html"));
        html.sendKeys(Keys.CONTROL, "0");
    }

}
