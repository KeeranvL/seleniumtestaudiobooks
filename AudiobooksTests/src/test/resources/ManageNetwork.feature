@run
Feature: Activate or deactivate connection with reseller



# Page interactions

  #Samen gedaan
  @Regressie
  Scenario: When a webshop has an active connection, the user has the possibility to end the connection.
    Given That the user is in the Webshops screen
    And The webshop has an active connection
    Then Deactivation button is visible

    #Marleen doet deze
  @Regressie
  Scenario: When the user ends the connection with end date tomorrow, the end date is filled with tomorrow
    Given That the user is in the Webshops screen
    And The webshop has an active connection
    When The user ends the connection
    Then The contract ends on the end date


  @Regressie
  Scenario: When a webshop has no active connection, the user has the possibility to create a connection.
    Given That the user is in the Webshops screen
    And The webshop has no active connection
    Then Activation button is visible


  @Regressie
  Scenario: A user can activate a connection, at earliest tomorrow
    Given That the user is in the Webshops screen
    And The webshop has no active connection
    When The user creates a connection
    Then The contract starts on the start date



  @Regressie
  Scenario: When a user clicks on the blue 'i' icon, the contract information shows
    Given User is in the Platforms screen
    When User clicks the blue 'i' icon next to LuisterBieb
    Then Popup shows with contract information

  @Regressie
  Scenario: A user can start and end the contract for day: tomorrow
    Given That the user is in the Webshops screen
    When The user activates the Kaleida connection with day tomorrow
    And The user ends the Kaleida connection with day tomorrow
    Then The start contract date for Kaleida should be tomorrow
    And The end contract date for Kaleida should be tomorrow

  @Regressie
  Scenario: A user cannot end the contract before the start date
    Given That the user is in the Webshops screen
    And The start date of the contract is in 3 days
    When User deactivates the contract by tomorrow
    Then Startdate of contract is filled instead

  @Regressie
  Scenario: A platform that doesn't allow publisher agreements, should see a special popup to cal CS
    Given That the user is in the Platforms screen
    When User clicks activate button next to Storytel
    Then Popup shows to call CS













