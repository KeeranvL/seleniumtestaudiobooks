@run
Feature: Activate or deactivate connection with reseller



# Page interactions

  #Samen gedaan
  @Regressie
  Scenario: When a webshop has an active connection, the user has the possibility to end the connection.
    Given That the user is in the Webshops screen
    And The webshop has an active connection
    Then Deactivation button is visible

    #Marleen doet deze
  @Regressie
  Scenario: When the user ends the connection with end date tomorrow, the end date is filled with tomorrow
    Given That the user is in the Webshops screen
    And The webshop has an active connection
    When The user ends the connection
    Then The contract ends on the end date


#  #Marleen doet deze
#  @Regressie
#  Scenario: When a webshop has no active connection, the user has the possibility to create a connection.
#    Given That the user is in the Webshops screen
#    And The webshop has no active connection
#    Then Activation button is visible
#
#    #Keeran doet deze
#  @Regressie
#  Scenario: When a webshop has no active connection, the user has the possibility to create a connection.
#    Given That the user is in the Webshops screen
#    And The webshop has no active connection
#    When The user creates a connection
#    Then The contract starts on the start date